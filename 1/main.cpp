#include "pch.h"

std::vector<int> cpuWork(int width, int height, bitmap_image & myImage)
{
    std::vector<unsigned  char> intensity(width * height);
    std::vector<int> MR (width * height);

    auto startCPUWork = clock();
    for (int x = 0; x < width; ++x)
    {
        for (int y = 0; y < height; ++y)
        {
            unsigned char R, G, B;
            myImage.get_pixel(x, y, R, G, B);
            int sum = R;
            sum += G;
            sum += B;
            intensity[y * width + x] = sum / 3;
        }
    }

    int filterSobelX[3][3] = {
            { -1, 0, 1 },
            { -2, 0, 2 },
            { -1, 0, 1 },
    };
    int filterSobelY[3][3] = {
            { -1, -2, -1 },
            { 0, 0, 0 },
            { 1, 2, 1 },
    };
    int MRmax = 0;

    for (int y = 1; y < height - 1; ++y)
    {
        for (int x = 1; x < width - 1; ++x)
        {
            int MX = 0;
            int MY = 0;
            for (int offsetY = -1; offsetY <= 1; ++offsetY)
            {
                for (int offsetX = -1; offsetX <= 1; ++offsetX)
                {
                    int intensityX = x + offsetX;
                    int intensityY = y + offsetY;
                    int sobelX = 1 + offsetX;
                    int sobelY = 1 + offsetY;
                    MX += filterSobelX[sobelX][sobelY] * intensity[intensityY * width + intensityX];
                    MY += filterSobelY[sobelX][sobelY] * intensity[intensityY * width + intensityX];
                }
            }

            double q = MX*MX + MY*MY;
            MR[y * width + x] = (int)sqrt(q);
            if (MRmax < MR[y * width + x])
                MRmax = MR[y * width + x];
        }
    }

    for (int x = 1; x < width; ++x)
    {
        for (int y = 1; y < height; ++y)
        {
            MR[y * width + x] = MR[y * width + x] * 255 / MRmax;
        }
    }

    auto endCPUWork = clock();
    std::cout << "CPU rendering time: " << ((double) (endCPUWork - startCPUWork)) / CLOCKS_PER_SEC << std::endl;

    return MR;
}

std::vector<int> openCLWork(int width, int height, bitmap_image & myImage)
{
    auto [context, device] = DeviceContext::getDefaultGPU();

    size_t groupSize = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();

    size_t imageInputSize  = width * height * 3;
    size_t pixelCount      = groupSize * ((width * height + groupSize - 1) / groupSize);
    size_t MRMaxBufferSize = (pixelCount / groupSize) * sizeof(int);

    std::vector<int> imageOutputData (pixelCount);
    size_t imageOutputSize = imageOutputData.size() * sizeof (int);

    int MRmax = 255;

    auto buffRgbInfo = cl::Buffer(context,
                                  CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                                  imageInputSize,
                                  (void *)myImage.data());

    auto buffIntensity = cl::Buffer(context,
                                    CL_MEM_READ_WRITE,
                                    pixelCount);

    auto buffMRMax = cl::Buffer(context,
                                CL_MEM_READ_WRITE,
                                MRMaxBufferSize);

    auto buffOutImg = cl::Buffer(context,
                                 CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                 imageOutputSize,
                                 imageOutputData.data());


    cl::Program program (context, sobelProgram);
    program.build({ device });

    cl::CommandQueue queue(context, device, 0);

    auto startGPUWork = clock();
    cl::Kernel kernelIntensity(program, "calculateIntensity");
    kernelIntensity.setArg(0, buffRgbInfo);
    kernelIntensity.setArg(1, buffIntensity);
    queue.enqueueNDRangeKernel(kernelIntensity, cl::NullRange, cl::NDRange(width, height));


    cl::Kernel kernelMR(program, "calculateMR");
    kernelMR.setArg(0, buffIntensity);
    kernelMR.setArg(1, buffOutImg);
    queue.enqueueNDRangeKernel(kernelMR, cl::NullRange, cl::NDRange(width, height));


    cl::Kernel kernelMRMax(program, "findMRmaxReduce");
    kernelMRMax.setArg(0, buffOutImg);
    kernelMRMax.setArg(1, buffMRMax);
    for(size_t currCount = pixelCount; currCount > 1;)
    {
        queue.enqueueNDRangeKernel(kernelMRMax, cl::NullRange, cl::NDRange(currCount), cl::NDRange(groupSize));
        kernelMRMax.setArg(0, buffMRMax);
        kernelMRMax.setArg(1, buffMRMax);

        currCount = (currCount + groupSize - 1 ) / groupSize;
    }

    queue.enqueueReadBuffer(buffMRMax, true, 0, sizeof(int), &MRmax);

    cl::Kernel kernelApplyMR(program, "applyMRmax");
    kernelApplyMR.setArg(0, buffOutImg);
    kernelApplyMR.setArg(1, MRmax);
    queue.enqueueNDRangeKernel(kernelApplyMR, cl::NullRange, cl::NDRange(width, height));

    queue.finish();

    auto endGPUWork = clock();
    std::cout << "GPU rendering time: " << ((double) (endGPUWork - startGPUWork)) / CLOCKS_PER_SEC << std::endl;

    queue.enqueueReadBuffer(buffOutImg, true, 0, imageOutputSize, imageOutputData.data());

    return imageOutputData;
}

void saveImage (int width, int height, std::vector<int> & MR, const char * fileName)
{
    bitmap_image outImg(width, height);
    auto * data = const_cast<unsigned char *>(outImg.data());
    for(int i = 0; i < width * height; ++i)
    {
        data[i * 3 + 0] = MR[i];
        data[i * 3 + 1] = MR[i];
        data[i * 3 + 2] = MR[i];
    }

    outImg.save_image(fileName);
}

int main()
{
    bitmap_image myImage("cat.bmp");
    int width = myImage.width();
    int height = myImage.height();
    std::cout << "height: " << height << "px" << " \nwidth: " << width << "px" << std::endl;

    try
    {
        auto MR = cpuWork(width, height, myImage);
        saveImage(width, height, MR, "catSobel.bmp");

        auto MR2 = openCLWork(width, height, myImage);
        saveImage(width, height, MR2, "catSobel2.bmp");
    }
    catch (cl::BuildError const &e)
    {
        std::cerr << "Build OpenCl error: " << e.what() << std::endl;
        for(auto &[dev, log] : e.getBuildLog())
        {
            std::cerr << dev.getInfo<CL_DEVICE_NAME>() << std::endl;
            std::cerr << log << std::endl;
        }
        return -1;
    }
    catch (cl::Error const &e)
    {
        std::cerr << "OpenCl exception with code = " << e.err() << " :" << e.what() << std::endl;
        return -1;
    }
    catch (std::exception const &e)
    {
        std::cerr << e.what() << std::endl;
        return -1;
    }

    return 0;
}