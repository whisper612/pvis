//
// Created by mrway on 09.10.2021.
//

#ifndef INC_1_SOBEL_H
#define INC_1_SOBEL_H

#define TOSTRING(...) #__VA_ARGS__

static constexpr const char *sobelProgram = TOSTRING(
        kernel void calculateIntensity (global unsigned char *image,
                                        global unsigned char *intensity)
        {
            size_t x = get_global_id(0);
            size_t y = get_global_id(1);
            size_t width = get_global_size(0);

            size_t index = y * (width * 3) + x * 3;
            int sum = image[index + 0];
            sum += image[index + 1];
            sum += image[index + 2];

            intensity[y * width + x] = sum / 3;
        }

        kernel void calculateMR (global unsigned char *intensity,
                                 global int *MR)
        {
            int filterSobelX[3][3] = {
                    { -1, 0, 1 },
                    { -2, 0, 2 },
                    { -1, 0, 1 },
            };
            int filterSobelY[3][3] = {
                    { -1, -2, -1 },
                    { 0, 0, 0 },
                    { 1, 2, 1 },
            };
            size_t x = get_global_id(0);
            size_t y = get_global_id(1);
            size_t width = get_global_size(0);
            size_t height = get_global_size(1);

            if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                return;

            int MX = 0;
            int MY = 0;
            for (int offsetY = -1; offsetY <= 1; ++offsetY)
            {
                for (int offsetX = -1; offsetX <= 1; ++offsetX)
                {
                    int intensityX = x + offsetX;
                    int intensityY = y + offsetY;
                    int sobelX = 1 + offsetX;
                    int sobelY = 1 + offsetY;
                    MX += filterSobelX[sobelX][sobelY] * intensity[intensityY * width + intensityX];
                    MY += filterSobelY[sobelX][sobelY] * intensity[intensityY * width + intensityX];
                }
            }
            double q = MX*MX + MY*MY;
            MR[y * width + x] = (int) sqrt(q);
        }

        kernel void findMRmaxReduce (global int *MR, global int *MRMax)
        {
            int MRIndex = get_global_id(0);
            int localIndex = get_local_id(0);
            int localSize = get_local_size(0);

            local int workGroupMax[256];
            workGroupMax[localIndex] = MR[MRIndex];
            barrier(CLK_LOCAL_MEM_FENCE);

            for (int i = localSize / 2; i >= 1; i /= 2)
            {
                if (localIndex < i)
                {
                    if (workGroupMax[localIndex] < workGroupMax[localIndex + i])
                        workGroupMax[localIndex] = workGroupMax[localIndex + i];
                }
                barrier(CLK_LOCAL_MEM_FENCE);
            }

            if (localIndex == 0)
                MRMax[MRIndex / localSize] = workGroupMax[localIndex];
        }

        kernel void applyMRmax(global int *MR,
                               int MRmax)
        {
            size_t x = get_global_id(0);
            size_t y = get_global_id(1);
            size_t width = get_global_size(0);

            MR[y * width + x] = MR[y * width + x] * 255 / MRmax;
        });

#endif //INC_1_SOBEL_H
