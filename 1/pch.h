//
// Created by mrway on 03.10.2021.
//

#ifndef INC_1_PCH_H
#define INC_1_PCH_H

#include <iostream>

#include "bitmap_image.hpp"

#include <CL/cl2.hpp>

#include "sobel.h"

#include "utils.h"

#endif //INC_1_PCH_H
