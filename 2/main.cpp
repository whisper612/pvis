#include "pch.h"

std::vector<int> levelFilling(int size)
{
    std::vector<int> pyramid(size * size);

    for (int i = 0; i < pyramid.size(); ++i)
    {
        pyramid[i] = 1 + std::rand() / ((RAND_MAX + 1u) / 99);
    }

    return pyramid;
}

std::vector<std::vector<int>> createPyramid(int level, int size)
{
    std::vector<std::vector<int>> pyramids(level);
    for (int i = 0; i < level; ++i)
    {
        pyramids[i] = levelFilling(size);
        size /= 2;
    }

    return pyramids;
}

int findMax(std::vector<std::vector<int>> const & pyramids)
{
    int max = 0;
    for (int level = 0; level < pyramids.size(); ++level)
    {
        #pragma omp parallel for
        for (int i = 0; i < pyramids[level].size(); ++i)
        {
            if (pyramids[level][i] > max)
                max = pyramids[level][i];
        }
    }

    return max;
}

void divisionOnMax(std::vector<std::vector<int>> & pyramids, int max)
{
    for (int level = 0; level < pyramids.size(); ++level)
    {
        #pragma omp parallel for
        for (int i = 0; i < pyramids[level].size(); ++i)
        {
            pyramids[level][i] = pyramids[level][i] / max;
        }
    }
}

void work(int d, int level, int threads)
{
    omp_set_num_threads(threads);

    int n = pow(2, d);
    auto pyramids = createPyramid(level, n);

    auto startCPUWork = clock();
    int max = findMax(pyramids);
    divisionOnMax(pyramids, max);
    auto endCPUWork = clock();

    std::cout << "CPU " << threads << " thread rendering time for D1 = " << d << ": "
        << ((double) (endCPUWork - startCPUWork)) / CLOCKS_PER_SEC << std::endl;
}

int main()
{
    int level = 6;

    work(12, level, 1);
    work(13, level, 1);
    work(14, level, 1);

    for(int threads = 2; threads <= 12; threads++)
    {
        for(int i = 0; i < 5; i++)
        {
            work(12, level, threads);
        }
        for(int i = 0; i < 5; i++)
        {
            work(13, level, threads);
        }
        for(int i = 0; i < 5; i++)
        {
            work(14, level, threads);
        }
    }

    return 0;
}
