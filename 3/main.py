import csv
import math
import copy
import random
import matplotlib.pyplot as plt
from mpi4py import MPI


Knowledge_MAP = {
    "very_low": 0,
    "Low": 0.33,
    "Middle": 0.66,
    "High": 1
}


K_MEANS_LAUNCH = 20000
CLASS_INDEX = 5
FILTER_DIM = (CLASS_INDEX, )


K = len(Knowledge_MAP)


def read_dataset():
    dataset = []
    with open("Knowledge.csv") as fd:
        for row in csv.reader(fd, delimiter=';'):
            dataset.append(row)

    del dataset[0]
    for i in range(len(dataset)):
        dataset[i][-1] = Knowledge_MAP[dataset[i][-1]]

    for c in range(len(dataset[0])):
        for row in dataset:
            row[c] = float(row[c])

    return dataset


def save_cluster(data):
    centroids = data["centroids"]
    clusters = data["clusters"]
    index = data["mb-index"]

    with open("result_ds1.res", "w") as fd:
        fd.write(f"MB-Index: {index}\n\n")

        for i in range(len(clusters)):
            fd.write(f"\n------------------------------------\n")
            fd.write(f"{centroids[i]}")
            fd.write(f"\n------------------------------------\n")

            for x in clusters[i]:
                fd.write(f"{x}\n")
            fd.write(f"====================================\n\n\n")


def calc_distance(x, y):
    dist = 0
    for i in range(len(x)):
        if i not in FILTER_DIM:
            dist += (x[i] - y[i]) ** 2
    return math.sqrt(dist)


def average(data):
    dim = len(data[0])
    avg_sum = [0.0] * dim
    for p in data:
        for i in range(dim):
            avg_sum[i] += p[i]
    for i in range(dim):
        avg_sum[i] /= len(data)
    return avg_sum



def take_unique_random(a, b, N):
    s = set()
    while len(s) != N:
        s.add(random.randint(a, b))
    return s


def k_means(dataset, k, max_iteration=1000, tolerance=0.0001):
    # выбираем начальные центры кластеров
    centroids = [dataset[i] for i in take_unique_random(0, len(dataset) - 1, k)]
    assert len(centroids) == k

    for _ in range(max_iteration):
        # на каждой итерации пересобираем все кластеры
        clusters = []
        for _ in range(k):
            clusters.append([])

        # определяем для каждой точки ближайший центр кластера
        for point in dataset:
            distances = [calc_distance(point, centroid) for centroid in centroids]
            cluster_number = distances.index(min(distances))
            clusters[cluster_number].append(point)

        previous_centroids = copy.deepcopy(centroids)

        # для каждого кластера вычисляем новый центр
        for i in range(k):
            centroids[i] = average(clusters[i])

        # если центры кластеров сдвинулись незначительно, то выходим
        if all([calc_distance(centroids[i], previous_centroids[i]) < tolerance for i in range(k)]):
            break

    return centroids, clusters


# Индекс Маулика Бундефая
def Maulik_Bandoypadhyay_index(centroids, clusters):
    K = len(centroids)
    dim = len(centroids[0])

    #нахождение центрального элемента
    N = 0
    X_centre = [0] * dim
    for c in clusters:
        N += len(c)
        for p in c:
            for i in range(dim):
                X_centre[i] += p[i]
    for i in range(dim):
        X_centre[i] /= N
    
    #нахождение сумма расстояний от каждого элемента до центрального E1
    Sum_distance = 0
    for c in clusters:
        for p in c:
            Sum_distance += calc_distance(p, X_centre)

    #нахождение суммы расстояний между центрами кластеров Ec
    Sum_distance_in_clusters = 0
    for cluster in range(K):
        for x in clusters[cluster]:
            Sum_distance_in_clusters += calc_distance(centroids[cluster], x)

    #максимальное расстояние между кластерами
    max_dist = calc_distance(centroids[0], centroids[1])
    for i in range(K):
        for j in range(K):
            if i == j:
                continue
            dist = calc_distance(centroids[i], centroids[j])
            max_dist = max(max_dist, dist)

    #расчёт самого индекса
    Maulik_Bandoypadhyay = (1 / len(clusters) * Sum_distance / Sum_distance_in_clusters * max_dist) ** 2

    return Maulik_Bandoypadhyay


def main_mpi():
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    
    if rank == 0:
        ds = read_dataset()
        reqs = [comm.isend(ds, i, tag=1) for i in range(1, size)]
        for r in reqs:
            r.wait()
    else:
        req = comm.irecv(source=0, tag=1)
        ds = req.wait()

    result = None
    for _ in range(K_MEANS_LAUNCH):
        centroids, clusters = k_means(ds, K, max_iteration=1000)
        index = Maulik_Bandoypadhyay_index(centroids, clusters)

        # MB-index чем больше, тем лучше
        if result is None or index > result["mb-index"]:
            result = {
                "mb-index": index,
                "centroids": centroids,
                "clusters": clusters
            }    

    index_max = comm.reduce(result["mb-index"], op=MPI.MAX, root=0)
    index_max = comm.bcast(index_max, root=0)

    if index_max == result["mb-index"]:
        print(f"Saving cluster in process {rank}")
        save_cluster(result)

    print(f"process {rank} exit.")
    comm.barrier()
    MPI.Finalize()

if __name__ == "__main__":
    main_mpi()